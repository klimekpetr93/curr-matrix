import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import { compareCurrencieRates } from '../utillities';
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    currencies: ['CZK', 'EUR', 'USD', 'GBP', 'CAD', 'HKD', 'CHF', 'AUD', 'INR'].sort(),
    currenciesRates: null,
  },
  mutations: {
    CLEAR_RATES(state) {
      state.currenciesRates = null;
    },
    SET_RATES(state, rates) {
      if (!state.currenciesRates) {
        state.currenciesRates = {
          date: rates['date'],
          rates: [{ base: rates['base'], ...rates['rates'] }],
        };
      } else {
        rates['base'] === 'EUR' ? (rates['rates'] = { ...rates['rates'], EUR: 1 }) : null;
        state.currenciesRates.rates.push({ base: rates['base'], ...rates['rates'] });
        state.currenciesRates.rates.length == state.currencies.length
          ? state.currenciesRates.rates.sort(compareCurrencieRates)
          : null;
      }
    },
  },
  actions: {
    fetchCurrRates({ commit }, { currencies, date }) {
      let url = 'https://api.exchangeratesapi.io/' + date + '?base=' + currencies;

      date == new Date().toISOString().split('T')[0]
        ? (url = 'https://api.exchangeratesapi.io/latest?base=' + currencies)
        : null;

      axios
        .get(url)
        .then((response) => {
          commit('SET_RATES', response.data);
        })
        .catch((error) => {
          console.log(error.statusText);
        });
    },
    clearCurrRates({ commit }) {
      commit('CLEAR_RATES');
    },
  },
});

export default store;
