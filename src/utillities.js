export const compareCurrencieRates = (a, b) => {
 
  const A = a.base.toUpperCase();
  const B = b.base.toUpperCase();

  let comparison = 0;
  if (A > B) {
    comparison = 1;
  } else if (A < B) {
    comparison = -1;
  }
  return comparison;
};
